// generate a number
function getRandomNumber(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// give me that number
console.log(getRandomNumber(15, 20));
